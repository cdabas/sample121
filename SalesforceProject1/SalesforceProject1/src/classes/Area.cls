public interface Area
{
	double areaShape();
    double circlearea();
    double rectanglearea();
}